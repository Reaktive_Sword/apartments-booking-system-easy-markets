﻿using ABSystem.Model;

namespace ABSystem.Helper
{
	public static class Calculator
	{
        public static double CalculateTotalCost(
            double basePricePerDay,
            DateTime startDate,
            DateTime endDate,
            List<SpecialPeriod> priceIncreasePeriods)
        {
            double totalCost = 0;  

            while (startDate <= endDate)
            {
                double dailyPrice = basePricePerDay;

                foreach (var period in priceIncreasePeriods)
                {
                    if (
                        (startDate.Month >= period.from.Month && startDate.Day >= period.from.Day)
                        || (startDate.Month <= period.to.Month && startDate.Day <= period.to.Month))
                    {
                        dailyPrice = dailyPrice * (1.00 + (period.percentage / 100.00));
                        break;
                    }
                }

                totalCost += dailyPrice;
                startDate = startDate.AddDays(1);
            }

            return Math.Round(totalCost, 2);
        }

        public static BookedApartment FindAvailablePeriods(
        DateTime userStartDate,
        DateTime userEndDate,
        List<BookedApartment> bookings)
        {
            var nearAnswers = new List<BookedApartment>();
            var groups = bookings
                .OrderBy(x => x.to)
                .GroupBy(x => x.apartmentId)
                .ToDictionary(x => x.Key, x => x.ToList());

            var period = userEndDate - userStartDate;

            foreach (var group in groups)
            {
                if (group.Value.Count == 1)
                {
                    var from = group.Value.First().to.AddDays(1);
                    var to = from + period;
                    nearAnswers.Add(
                        new BookedApartment(
                            1,
                            group.Key,
                            from,
                            to));
                }
                else
                {
                    for (int i = 0; i < group.Value.Count - 2; i++)
                    {
                        if (group.Value[i].to < userStartDate && group.Value[i+1].from > userEndDate)
                        {
                            nearAnswers.Add(
                        new BookedApartment(
                            1,
                            group.Key,
                            userStartDate,
                            userEndDate));
                        }
                    }

                    if (!nearAnswers.Any(x => x.apartmentId == group.Key))
                    {
                        var from = group.Value.Last().to.AddDays(1);
                        var to = from + period;
                        nearAnswers.Add(
                            new BookedApartment(
                                1,
                                group.Key,
                                from,
                                to));
                    }
                }
                
            }

            return nearAnswers.OrderBy(x => x.from).First();
        }
    }
}