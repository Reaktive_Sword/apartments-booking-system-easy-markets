﻿using System;
namespace ABSystem.Model
{
	public record SpecialPeriod(
		DateTime from,
		DateTime to,
		double percentage);
}

