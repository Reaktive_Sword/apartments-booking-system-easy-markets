﻿using System;
namespace ABSystem.Model
{
	public record BookedApartment(
		uint id,
		uint apartmentId,
		DateTime from,
		DateTime to);
}

