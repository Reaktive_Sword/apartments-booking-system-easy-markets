﻿using System;
namespace ABSystem.Model
{
	public record Apartment(
		uint id,
		uint numberOfRooms,
		double price);
}

