﻿using System;
using ABSystem.Helper;

namespace ABSystem.Model
{
	public record User(
		uint id,
		string name,
		string password,
		RoleEnum role,
		uint discount);
}

