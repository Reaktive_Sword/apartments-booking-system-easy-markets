﻿using System;
using System.Globalization;
using ABSystem.Helper;
using ABSystem.Model;
using ABSystem.Service;
using Microsoft.Extensions.Hosting;

namespace ABSystem
{
	public sealed class Menu
	{
        private readonly BookingService _bookingService;

        const string format = "dd.MM.yyyy";

        public Menu(
            BookingService bookingService)
		{
            _bookingService = bookingService;
            Start();
        }

        public void Start()
        {
            var user = FindUser();
            switch (user.role)
            {
                case RoleEnum.customer:
                    MenuForCustomer(user);
                    break;
                case RoleEnum.admin:
                    MenuForAdmin(user);
                    break;
                default:
                    break;
            }
        }

        private User FindUser()
        {
            Console.WriteLine("Name");
            var name = Console.ReadLine();
            Console.WriteLine("Password");
            var password = Console.ReadLine();

            return AuthorizationService.CheckUser(name, password);
        }

        private void MenuForCustomer(User user)
        {
            while (true)
            {
                var menu = @"
book - Find and book new room";

                Console.WriteLine(menu);

                var answer = Console.ReadLine();

                switch (answer)
                {
                    case "book":
                        Show(user);
                        break;
                    case "exit":
                    default:
                        Console.WriteLine("Bye-bye, Have a good day");
                        Console.ReadKey();
                        return;
                }
            }
        }

        private void MenuForAdmin(User user)
        {
            while (true)
            {
                var menu = @"
book - Find and book new room
booked - Booked for period";

                Console.WriteLine(menu);

                var answer = Console.ReadLine();

                switch (answer)
                {
                    case "book":
                        Show(user);
                        break;
                    case "booked":
                        BookedForThisPeriod();
                        break;
                    case "exit":
                    default:
                        Console.WriteLine("Bye-bye, Have a good day");
                        Console.ReadKey();
                        return;
                }
            }
        }

        private void BookedForThisPeriod()
        {
            Console.WriteLine("Perid from. Example: dd.MM.YYYY");
            string dateStringFrom = Console.ReadLine();

            DateTime.TryParseExact(
                dateStringFrom,
                format,
                CultureInfo.InvariantCulture,
                DateTimeStyles.None,
                out var from);

            Console.WriteLine("Perid to. Example: dd.MM.YYYY");
            string dateStringTo = Console.ReadLine();

            DateTime.TryParseExact(
                dateStringTo,
                format,
                CultureInfo.InvariantCulture,
                DateTimeStyles.None,
                out var to);

            _bookingService.ShowBookedApartments(from, to);
        }

        private void Show(User user)
        {
            Console.WriteLine("Perid from. Example: dd.MM.YYYY");
            string dateStringFrom = Console.ReadLine();

            DateTime.TryParseExact(
                dateStringFrom,
                format,
                CultureInfo.InvariantCulture,
                DateTimeStyles.None,
                out var from);

            Console.WriteLine("Perid to. Example: dd.MM.YYYY");
            string dateStringTo = Console.ReadLine();

            DateTime.TryParseExact(
                dateStringTo,
                format,
                CultureInfo.InvariantCulture,
                DateTimeStyles.None,
                out var to);

            Console.WriteLine("Number of rooms (1, 2, 3). Example: 1");
            uint rooms = Convert.ToUInt32(Console.ReadLine());

            var period = _bookingService.ShowFreeApartments(from, to, rooms, user);

            Console.WriteLine("Choose name. Example: 1");
            uint id = Convert.ToUInt32(Console.ReadLine());
            _bookingService.Book(id, period.fromNew, period.toNew);
        }
	}
}

