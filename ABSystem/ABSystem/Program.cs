﻿// See https://aka.ms/new-console-template for more information
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ABSystem;
using ABSystem.Service;

HostApplicationBuilder builder = Host.CreateApplicationBuilder(args);

builder.Services.AddSingleton<Menu>();
builder.Services.AddSingleton<BookingService>();
builder.Services.AddScoped<EmailService>();

using IHost host = builder.Build();

host.Services.GetRequiredService<Menu>();


