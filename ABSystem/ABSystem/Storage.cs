﻿using System;
using ABSystem.Model;

namespace ABSystem
{
	public static class Storage
	{
		private static List<Apartment> Apartments = new List<Apartment>
		{
            new Apartment(1, 1, 50),
            new Apartment(2, 1, 50),
            new Apartment(3, 1, 50),
            new Apartment(4, 1, 50),
            new Apartment(5, 2, 60),
            new Apartment(6, 2, 60),
            new Apartment(7, 2, 60),
            new Apartment(8, 3, 70),
            new Apartment(9, 3, 70),
            new Apartment(10, 3, 70),
        };

        private static List<User> Users = new List<User>
        {
            new User(1, "admin", "admin", Helper.RoleEnum.admin, 100),
            new User(100, "Test1", "test", Helper.RoleEnum.customer, 100),
            new User(101, "OldTest1", "oldTest", Helper.RoleEnum.customer, 90),
        };


        private static List<SpecialPeriod> SpecialPeriods = new List<SpecialPeriod>
        {
            new SpecialPeriod(new DateTime(1, 10, 1), new DateTime(1, 4, 1), 10),
            new SpecialPeriod(new DateTime(1, 5, 1), new DateTime(1, 9, 1), 20),
        };

        private static List<BookedApartment> BookedApartments = new List<BookedApartment>
        { };

        public static List<Apartment> GetApartments()
		{
            try
            {
                return Apartments;
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                return null;
            }

        }

        public static void AddApartment(
            uint numberOfRooms,
            uint price,
            bool booked)
        {
            var maxId = GetApartments()?.Max(x => x.id) ?? 0;
            Apartments.Add(new Apartment(maxId+1, numberOfRooms, price));
        }

        public static List<SpecialPeriod> GetSpecialPeriods()
        {
            return SpecialPeriods;

        }

        public static void AddSpecialPeriod(
            DateTime from,
            DateTime to,
            uint discount)
        {
            SpecialPeriods.Add(new SpecialPeriod(from, to, discount));
        }

        public static List<BookedApartment> GetBookedApartments(DateTime from, DateTime to)
        { 
            return BookedApartments
                .Where(x => x.from <= to && x.to >= from)
                .ToList();
        }

        public static List<BookedApartment> GetBookedApartments(DateTime from)
        {
            return BookedApartments
                .Where(x => x.to >= from)
                .ToList();
        }

        public static List<BookedApartment> GetBookedApartments()
        {
            return BookedApartments
                .ToList();
        }

        public static void AddBookedApartment(
            uint apartmentId,
            DateTime from,
            DateTime to)
        {
            var collection = GetBookedApartments().ToList();
            uint maxId = 0;
            if (collection.Count != 0)
            {
                maxId = collection.Max(x => x.id);
            }
            
            BookedApartments.Add(new BookedApartment(maxId + 1, apartmentId, from, to));
        }

        public static User GetUser(
            string name,
            string password)
        {
            return Users.FirstOrDefault(x => x.name == name && x.password == password);
        }
    }
}

