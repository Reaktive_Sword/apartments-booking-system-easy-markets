﻿using ABSystem.Helper;
using ABSystem.Model;

namespace ABSystem.Service
{
	public class BookingService: IBookingService
    {
		private readonly EmailService _emailService;
		public BookingService(EmailService emailService)
		{
			_emailService = emailService;
        }

		public async void Book(uint apartmentId, DateTime from, DateTime to)
		{
            List<BookedApartment> bookedApartments = Storage.GetBookedApartments(from, to);
			if (bookedApartments.Any(x => x.apartmentId == apartmentId))
			{
				Console.WriteLine();

                Console.WriteLine("Sorry, but this apartment is anavailable");
				return;
			}
            Storage.AddBookedApartment(apartmentId, from, to);
			_emailService.SendToCustomer();
        }

		public void ShowBookedApartments(DateTime from, DateTime to)
		{
            var apartments = Storage.GetApartments();

			if (apartments == null)
			{
				Console.WriteLine("Error: there aren't any apartments");
				return;
			}

            List<BookedApartment> bookedApartments = Storage.GetBookedApartments(from, to);

            var freeApartments = apartments
                .Where(x => bookedApartments.Any(y => y.apartmentId == x.id));

            foreach (var bookedapartment in bookedApartments)
            {
				var apartment = apartments.First(x => x.id == bookedapartment.apartmentId);

                Console.WriteLine();
                Console.WriteLine(@$"name: {apartment.id}
rooms: {apartment.numberOfRooms},
price per day: {apartment.price}
from: {bookedapartment.from}
to: {bookedapartment.to}");

            }
        }

        public (DateTime fromNew, DateTime toNew) ShowFreeApartments(
			DateTime from,
			DateTime to,
			uint numberOfRooms,
			User user)
        {
            var apartments = Storage.GetApartments()?
				.Where(x => x.numberOfRooms == numberOfRooms)
				.ToList();

            if (apartments == null)
            {
                throw new Exception("Error: there aren't any apartments");
            }

            var specialPeriods = Storage.GetSpecialPeriods();

			List<BookedApartment> bookedApartments = Storage.GetBookedApartments(from);

			var freeApartments = apartments
				.Where(x => !bookedApartments.Any(y => y.apartmentId == x.id))
				.ToList();

			if (freeApartments.Count == 0)
			{
				var closeApartment = bookedApartments
					.Where(x => x.to > from)
					.Where(x => apartments.Any(y => y.id == x.apartmentId))
					.ToList();
				var answer = Calculator.FindAvailablePeriods(from, to, closeApartment);
                var price = apartments.First(x => x.numberOfRooms == numberOfRooms).price;
				from = answer.from;
				to = answer.to;

                freeApartments.Add(new Apartment(answer.apartmentId, numberOfRooms, price));
            }
	

			foreach (var apartment in freeApartments)
			{
				var pricePerDay = (apartment.price * user.discount) / 100;

                var price = Calculator.CalculateTotalCost(
                    pricePerDay,
					from,
					to,
					specialPeriods);
				Console.WriteLine();
                Console.WriteLine(@$"name: {apartment.id},
rooms: {apartment.numberOfRooms},
price per day: {pricePerDay},
price: {price}
from: {from}
to: {to}");
			}

			return (from, to);
        }
    }
}

