﻿using System;
using ABSystem.Helper;
using ABSystem.Model;

namespace ABSystem.Service
{
	public static class AuthorizationService
	{
		public static User CheckUser(string name, string password)
		{
			User user = Storage.GetUser(name, password);
			if (user == null)
			{
				Console.WriteLine("Sorry we don't know you");
			}
			return user;
		}
    }
}

